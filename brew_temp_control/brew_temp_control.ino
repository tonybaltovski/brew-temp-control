#include <Wire.h>
#include <LCD.h>
#include <LiquidCrystal_I2C.h>  // F Malpartida's NewLiquidCrystal library
//Download: https://bitbucket.org/fmalpartida/new-liquidcrystal/downloads
// Move original LiquidCrystal library elsewhere, copy this in it's place 
#define I2C_ADDR    0x27 
//---(Following are the PCF8574 pin assignments to LCD connections )----
// This are different than earlier/different I2C LCD displays
#define BACKLIGHT_PIN  3
#define En_pin  2
#define Rw_pin  1
#define Rs_pin  0
#define D4_pin  4
#define D5_pin  5
#define D6_pin  6
#define D7_pin  7

#define  LED_OFF  1
#define  LED_ON  0


#include <OneWire.h>
#include <DallasTemperature.h>
#include <Keypad.h>
#include <Time.h>

#define ONE_WIRE_BUS 10 // Data wire is plugged into port 10 on the Arduino
#define RELAY_IN1 11
#define RELAY_IN2 12

// Setup a oneWire instance to communicate with any OneWire devices (not just Maxim/Dallas temperature ICs)
OneWire oneWire(ONE_WIRE_BUS);
// Pass our oneWire reference to Dallas Temperature. 
DallasTemperature sensors(&oneWire);
// arrays to hold device address
DeviceAddress insideThermometer;

char rawSetTemp[2];
int rawSetTempIndex = 26;
int setTemp = 26;
int currentTemp;
int tol = 2;
int controlTime = 1; // min
int cycles = 0;


time_t elapsedTime;

bool printOn = true;
int presses = 0;
int pressPos = 0;

int dTperDay = 0;
int currentDay = 0;
int oldDay = 0;

/////////////////KEYPAD/////////////////
const byte ROWS = 4; //four rows
const byte COLS = 4; //four columns
//define the cymbols on the buttons of the keypads
char hexaKeys[ROWS][COLS] = {
  {
    '1','2','3','A'}
  ,
  {
    '4','5','6','B'}
  ,
  {
    '7','8','9','C'}
  ,
  {
    '*','0','#','D'}
};
byte rowPins[ROWS] = {
  9, 8, 7, 6}; //connect to the row pinouts of the keypad
byte colPins[COLS] = {
  5, 4, 3, 2}; //connect to the column pinouts of the keypad

//initialize an instance of class NewKeypad
Keypad customKeypad = Keypad( makeKeymap(hexaKeys), rowPins, colPins, ROWS, COLS); 
//initialize the i2c lcd
LiquidCrystal_I2C  lcd(I2C_ADDR,En_pin,Rw_pin,Rs_pin,D4_pin,D5_pin,D6_pin,D7_pin);


void setup(void)
{
  lcd.begin (16,2);  // initialize the lcd 
  // Switch on the backlight
  lcd.setBacklightPin(BACKLIGHT_PIN,POSITIVE);
  lcd.setBacklight(LED_ON);
  lcd.clear();
  lcd.home();
  pinMode(RELAY_IN1, OUTPUT);
  digitalWrite(RELAY_IN1, HIGH); //HIGH is OFF
  pinMode(RELAY_IN2, OUTPUT);
  digitalWrite(RELAY_IN2, HIGH);
  insideThermometer = {0x28, 0x3A, 0x8B, 0xC7, 0x04, 0x00, 0x00, 0xA6};
  if (!sensors.getAddress(insideThermometer, 0)) 
  {
    lcd.setCursor(0,0);
    lcd.println("Unable to find address for Device 0"); 
  }
  sensors.setResolution(insideThermometer, 9);
  customKeypad.addEventListener(keypadEvent); // Add an event listener for this keypad
}


void loop(void)
{

  readTemp(); //Reads temp
  keyPress();
  if(minute(elapsedTime) % controlTime == 0)
    controlRelay();
  if(printOn)
    printLCD();

}

void printLCD(void)
{
  //Prints the desired data on the LCD screen
  lcd.backlight();  //Backlight ON if under program control
  lcd.setCursor(0,0);
  lcd.print("Ta:");
  lcd.print(currentTemp);
  lcd.setCursor(5,0);
  lcd.print(" Ts:");
  lcd.print(setTemp);
  lcd.setCursor(11,0);
  lcd.print(" T:");
  lcd.print(tol);
  lcd.setCursor(0,1);
  lcd.print("dT");
  lcd.print(dTperDay);
  lcd.print("m ");
  lcd.setCursor(5,1);
  elapsedTime = now();
  lcd.print("D");
  lcd.print(hour(elapsedTime)/24);  //The day acount is not working so I divided an integer by 24 which should give days
  lcd.setCursor(8,1);
  lcd.print("H");
  lcd.print(hour(elapsedTime));
  lcd.setCursor(11,1);
  lcd.print("M");
  lcd.print(minute(elapsedTime));
}


void readTemp(void)
{
  //Reands the temp in C
  sensors.requestTemperatures();
  currentTemp = sensors.getTempC(insideThermometer); 
}

void controlRelay(void)
{
  //Turns on or off the rely
  if (currentTemp >= (setTemp - tol))
  {
    digitalWrite(RELAY_IN1,LOW); //Turn on cooling 
    cycles++;
  }
  else if (currentTemp <= (setTemp + tol))
  {
    digitalWrite(RELAY_IN1,HIGH); //Turn off cooling
  }
}

void keyPress (void)
{
  //Gets key presss
  char customKey = customKeypad.getKey();
  delay(100);
}

void keypadEvent(KeypadEvent key){

  //Checks the keypress input
  switch (customKeypad.getState()){
  case PRESSED:
    if (key == 'A')
    {
      lcd.clear();
      lcd.setCursor(0,0);
      lcd.print("Enter 1st digit [0-9]");
      lcd.setCursor(0,1);
      printOn = false;
      presses++;
      pressPos = 1;
    }
    else if (key == 'B') 
    {
      lcd.clear();
      lcd.setCursor(0,0);
      lcd.print("Tolerance [0-9]");
      printOn = false;
      presses++;
      pressPos = 2;
    }
    else if (key == 'C') 
    {
      lcd.clear();
      lcd.setCursor(0,0);
      lcd.print("ControlTime[0-9]");
      printOn = false;
      presses++;
      pressPos = 3;
    }
    else if (key == 'D') 
    {
      lcd.clear();
      lcd.setCursor(0,0);
      lcd.print("ChangePerDay[0-9]");
      printOn = false;
      presses++;
      pressPos = 4;
    }
    else 
    {
      //lcd.print(key);
      presses++;
    }
    break;
  case HOLD:
    if (key == '*') {
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("Number of cooling cycles");
    lcd.setCursor(0,1);
    lcd.print(cycles);
    }
    break;
        if (key == '#') {
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("Control Time");
    lcd.setCursor(0,1);
    lcd.print(controlTime);
    }
    break;
  }
  
  if (pressPos == 1  && presses == 2)
  {
    rawSetTemp[0] = key;
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("Enter 2nd digit [0-9]");
    lcd.setCursor(0,1);
    lcd.print(rawSetTemp[0]);         
  }

  else if (pressPos == 1  && presses == 3)
  {
    rawSetTemp[1] = key;
    setTemp = atof(rawSetTemp);
    printOn = true;
    lcd.clear();
    presses = 0;
    pressPos = 0;
  }
  else if (pressPos == 2  && presses == 2)
  {
    printOn = true;
    tol = keytoValue(key);
    lcd.clear();
    presses = 0;
    pressPos = 0;
  }
  else if (pressPos == 3  && presses == 2)
  {
    printOn = true;
    controlTime = keytoValue(key);
    lcd.clear();
    presses = 0;
    pressPos = 0;
  }

  else if (pressPos == 4  && presses == 2)
  {
    printOn = true;
    dTperDay = keytoValue(key);
    lcd.clear();
    presses = 0;
    pressPos = 0;
  }

}

void dayTempChange(void)
{
  //Suppose to change the set temp per day but does not seem to work.  I believe it is because the day count doesn't work.
  currentDay = hour(elapsedTime)/24;
  if(currentDay > oldDay)
  {
    setTemp = setTemp + dTperDay;
    oldDay = currentDay;
  }
}

int keytoValue(char c)
{
  //Method to change the key press to a number.  TODO look for a easier way.
  switch (c) {
  case '1':
    return 1;
    break;
  case '2':
    return 2;
    break;
  case '3':
    return 3;
    break;
  case '4':
    return 4;
    break;
  case '5':
    return 5;
    break;
  case '6':
    return 6;
    break;
  case '7':
    return 7;
    break;
  case '8':
    return 8;
    break;
  case '9':
    return 9;
    break;
  case '0':
    return 0;
    break;

  }
}
















